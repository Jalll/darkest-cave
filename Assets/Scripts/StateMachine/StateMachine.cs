﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class StateMachine<T>
{
    private List<State<T>> _states = new List<State<T>>();
    private State<T> _currentState;

    public void Update()
    {
        _currentState?.Update();
    }
    
    public void LateUpdate()
    {
        _currentState?.LateUpdate();
    }

    public void AddState(State<T> newState)
    {
        _states.Add(newState);
        
        if (_currentState == null)
        {
            _currentState = newState;
        }
    }

    public void SetState<K>() where K : State<T>
    {
        Debug.Log(typeof(K).ToString());
        foreach (var state in _states.Where(state => state.GetType() == typeof(K)))
        {
            _currentState.Sleep();
            _currentState = state;
            _currentState.Awake();
        }
    }
}
