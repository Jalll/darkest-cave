﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class State<T>
{
    protected T _owner;
    protected StateMachine<T> _stateMachine;
    
    protected State(T owner, StateMachine<T> stateMachine)
    {
        _owner = owner;
        _stateMachine = stateMachine;
    }
    
    public abstract void Awake();
    public abstract void Update();
    public abstract void LateUpdate();
    public abstract void Sleep();
}
