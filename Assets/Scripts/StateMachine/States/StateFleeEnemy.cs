﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateFleeEnemy : State<Enemy>
{
    public StateFleeEnemy(Enemy owner, StateMachine<Enemy> stateMachine) : base(owner, stateMachine)
    {
    }

    public override void Awake()
    {
        
    }

    public override void Update()
    {
        
    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        
    }
}
