using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class StateChaseEnemy : State<Enemy>
{

    private GameObject _target;
    private Seek _seek;
    private float _attackRange;
    private float _seekTime;
    private float _seekTimer;
    private float _speed;
    public StateChaseEnemy(Enemy owner, StateMachine<Enemy> stateMachine, Seek seek, GameObject target, float attackRange, float seekTime, float speed) : base(owner, stateMachine)
    {
        _target = target;
        _seek = seek;
        _attackRange = attackRange;
        _seekTime = seekTime;
        _speed = speed;
    }

    public override void Awake()
    {
        _seek.SetTarget(_owner.target.transform);
        _seek.SetSpeed(_speed * 2);
        _target = _owner.target;
        _seekTimer = _seekTime;
    }

    public override void Update()
    {
        var distance = Vector3.Distance(_owner.transform.position, _target.transform.position);
        Debug.Log(_attackRange);
        if (distance <= _attackRange)
        {
            _stateMachine.SetState<StateAttackEnemy>();
            return;
        }

        _seekTimer -= Time.deltaTime;

        if (_seekTimer <= 0)
        {
            _stateMachine.SetState<StatePatrolEnemy>();
        }

    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        _seek.SetSpeed(_speed);
    }
}    