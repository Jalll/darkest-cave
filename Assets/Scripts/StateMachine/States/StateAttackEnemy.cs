﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class StateAttackEnemy : State<Enemy>
{
    private float _attackRange;
    private Sensor _sensor;
    private Player _player;
    private LayerMask _keyLayer;
    private Seek _seek;
    private float _baseSpeed;
    
    public StateAttackEnemy(
        Enemy owner, 
        StateMachine<Enemy> stateMachine, 
        Sensor sensor, 
        float attackRange, 
        LayerMask keyLayer,
        Seek seek,
        float baseSpeed) : base(owner, stateMachine)
    {
        _attackRange = attackRange;
        _sensor = sensor;
        _keyLayer = keyLayer;
        _seek = seek;
        _baseSpeed = baseSpeed;
    }

    public override void Awake()
    {
        _player = _owner.target.GetComponent<Player>();
        if (_player == null)
        {
            _stateMachine.SetState<StatePatrolEnemy>();
        }

        _seek.SetSpeed(_baseSpeed / 2);
    }

    public override void Update()
    {
        var distance = Vector3.Distance(_owner.target.transform.position, _owner.transform.position);
        if (distance < _attackRange)
        {
            _player.ForceDropKey();
        }

        var inProximity = _sensor.GetGameObjectsInProximity(_keyLayer, _owner.transform.position);
        if (inProximity.Count > 0)
        {
            foreach (var obj in inProximity)
            {
                var key = obj.GetComponent<Key>();
                if (key != null)
                {
                    _owner.SetTarget(key.gameObject);
                    _stateMachine.SetState<StateGrabKeyEnemy>();
                    return;
                }
            }
        }
        
        var inLineOfSight = _sensor.GetGameObjectsOnLineOfSight(_keyLayer, _owner.transform.position);
        if (inLineOfSight.Count > 0)
        {
            var key = inLineOfSight[0].GetComponent<Key>();
            if (key != null)
            {
                _owner.SetTarget(key.gameObject);
                _stateMachine.SetState<StateGrabKeyEnemy>();
                return;
            }
        }
        
        _stateMachine.SetState<StatePatrolEnemy>();

    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        _seek.SetSpeed(_baseSpeed);
    }
}
