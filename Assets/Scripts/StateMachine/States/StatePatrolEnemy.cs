﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Runtime.InteropServices;
using JetBrains.Annotations;
using UnityEditorInternal;
using UnityEngine;

public class StatePatrolEnemy : State<Enemy>
{

    private WaypointsPathScript _path;
    private Waypoint _currentTarget;
    private Seek _seek;
    private int _countWaypoints;
    private Sensor _sensor;
    private LayerMask _playerLayer;
    private RouletteWheelSelection<Waypoint> _roulette;

    private Stack<Waypoint> _currentPath;
    private Waypoint _goal;
    private bool _onPath;
    

    public StatePatrolEnemy(
        Enemy owner,
        StateMachine<Enemy> stateMachine,
        WaypointsPathScript path,
        Seek seek,
        Sensor sensor,
        LayerMask playerLayer,
        RouletteWheelSelection<Waypoint> roulette
    ) : base(owner, stateMachine)
    {
        _path = path;
        _seek = seek;
        _sensor = sensor;
        _playerLayer = playerLayer;
        _roulette = roulette;
    }

    public override void Awake()
    {
        var start = _path.GetNearestWaypoint(_owner.transform.position);
        if (start != null)
        {
            _onPath = false;
            _goal = _path._waypoints[0];
            _currentPath = _path.ExecuteAstar(start, _goal);

            _currentTarget = _currentPath.Pop();
            _seek.SetTarget(_currentTarget.transform);
        }
    }

    public override void Update()
    {
        if (Vector3.Distance(_currentTarget.transform.position, _owner.transform.position) < 1f)
        {
            _onPath = true;
            if (_currentPath.Count == 0)
            {
                var start = _currentTarget;
                var goal = _roulette.GetRouletWheelResult();
                _currentPath = _path.ExecuteAstar(start, goal);
            }
            _currentTarget = _currentPath.Pop();
            _seek.SetTarget(_currentTarget.transform);
        }

        var inProximity = _sensor.GetGameObjectsInProximity(_playerLayer, _owner.transform.position);
        if (inProximity.Count > 0)
        {
            var player = inProximity[0].GetComponent<Player>();
            if (player != null && _onPath)
            {
                _owner.SetTarget(player.gameObject);
                _stateMachine.SetState<StateChaseEnemy>();
            }
        }

        var inLineOfSight = _sensor.GetGameObjectsOnLineOfSight(_playerLayer, _owner.transform.position);
        if(inLineOfSight.Count > 0)
        {
            var player = inLineOfSight[0].GetComponent<Player>();
            if (player != null && _onPath)
            {
                _owner.SetTarget(player.gameObject);
                _stateMachine.SetState<StateChaseEnemy>();
            }
        }
    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        
    }
}
