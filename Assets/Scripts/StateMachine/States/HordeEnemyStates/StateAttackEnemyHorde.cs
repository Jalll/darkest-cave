using System.Collections.Generic;
using UnityEngine;

public class StateAttackEnemyHorde : State<Enemy>
{
    private Stack<HordeMinionEnemy> _minions;
    private float _attackSpeed;
    private Seek _seek;
    private Movement _movement;
    private Sensor _sensor;
    private LayerMask _playerLayer;
    
    private float _attackTimer;


    public StateAttackEnemyHorde(
        HordeEnemy owner, 
        StateMachine<Enemy> stateMachine
        , Stack<HordeMinionEnemy> minions, 
        float attackSpeed,
        Seek seek,
        Movement movement,
        Sensor sensor,
        LayerMask playerLayer) : base(owner, stateMachine)
    {
        _minions = minions;
        _attackSpeed = attackSpeed;
        _seek = seek;
        _movement = movement;
        _sensor = sensor;
        _playerLayer = playerLayer;
    }

    public override void Awake()
    {
        _seek.isActivated = false;
        _attackTimer = _attackSpeed;
    }

    public override void Update()
    {
        _movement.Rotate(_owner.target.transform.position, 8f);
        
        if (_minions.Count <= 0)
        {
            _stateMachine.SetState<StateHideEnemyHorde>();
        }

        _attackTimer -= Time.deltaTime;
        if (_attackTimer <= 0)
        {
            var minion = _minions.Pop();
            minion.Attack(_owner.target);
            _attackTimer = _attackSpeed;
        }
    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        _seek.isActivated = true;
    }
}