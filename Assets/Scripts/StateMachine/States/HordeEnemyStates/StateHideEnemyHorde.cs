﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateHideEnemyHorde : State<Enemy>
{
    private RouletteWheelSelection<Waypoint> _roulette;
    private Seek _seek;
    private WaypointsPathScript _path;
    
    
    private GameObject _currentTarget;
    private GameObject _goal;
    private float _hidingTimer;
    private List<Waypoint> _waypoints;
    private Stack<Waypoint> _currentPath;

    
    public StateHideEnemyHorde(
        HordeEnemy owner, 
        StateMachine<Enemy> stateMachine,
        Seek seek, 
        RouletteWheelSelection<Waypoint> roulette,
        WaypointsPathScript path) : base(owner, stateMachine)
    {
        _seek = seek;
        _roulette = roulette;
        _path = path;
    }
    
    public override void Awake()
    {
        var goal = _roulette.GetRouletWheelResult();
        var start = _path.GetNearestWaypoint(_owner.transform.position);

        if (start != null)
        {
            _currentPath = _path.ExecuteAstar(start, goal);
            _currentTarget = _currentPath.Pop().gameObject;
            _seek.SetTarget(_currentTarget.transform);
        }
    }

    public override void Update()
    {
        var distance = Vector3.Distance(_currentTarget.transform.position, _owner.transform.position);
        if (distance <= 1f)
        {
            if (_currentPath.Count == 0)
            {
                _stateMachine.SetState<StateRechargeMinionsEnemyHorde>();
            }
            else
            {
                _currentTarget = _currentPath.Pop().gameObject;
                _seek.SetTarget(_currentTarget.transform);
            }
        }
    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        
    }


}
