﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StateRechargeMinionsEnemyHorde : State<Enemy>
{

    private float _rechargeTime;
    private float _maxMinions;
    private Stack<HordeMinionEnemy> _minions;
    private Seek _seek;
    private HordeEnemy _hordeOwner;
    
    private float _rechargeTimer;
    
    public StateRechargeMinionsEnemyHorde(
        HordeEnemy owner, 
        StateMachine<Enemy> stateMachine,
        float rechargeTime,
        float maxMinions,
        Stack<HordeMinionEnemy> minions,
        Seek seek) : base(owner, stateMachine)
    {
        _rechargeTime = rechargeTime;
        _maxMinions = maxMinions;
        _minions = minions;
        _seek = seek;
    }

    public override void Awake()
    {
        _seek.isActivated = false;
        _rechargeTimer = _rechargeTime;
        _hordeOwner = _owner.GetComponent<HordeEnemy>();
    }

    public override void Update()
    {
        _rechargeTimer -= Time.deltaTime;
        _owner.transform.RotateAround(_owner.transform.position, _owner.transform.up, 1);
        if (_minions.Count >= _maxMinions)
        {
            _stateMachine.SetState<StatePatrolEnemyHorde>();
        }
        
        if (_rechargeTimer <= 0 && _hordeOwner != null)
        {
            _hordeOwner.SpawnMinion();
            _rechargeTimer = _rechargeTime;
        }
    }

    public override void LateUpdate()
    {
        
    }

    public override void Sleep()
    {
        _seek.isActivated = true;
    }
    
}
