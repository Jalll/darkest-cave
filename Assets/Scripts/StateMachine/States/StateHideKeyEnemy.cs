
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    public class StateHideKeyEnemy : State<Enemy>
    {
        private const int MAX_RADIUS = 50;

        private Inventory _inventory;
        private RouletteWheelSelection<Waypoint> _roulette;
        private Seek _seek;
        private float _hidingTime;
        private WaypointsPathScript _path;

        private GameObject _currentTarget;
        private GameObject _goal;
        private float _hidingTimer;
        private List<Waypoint> _waypoints;
        private Stack<Waypoint> _currentPath;
        private ChaseEnemy _chaseOwner;
        
        
        public StateHideKeyEnemy(
            Enemy owner, 
            StateMachine<Enemy> stateMachine, 
            Seek seek, 
            Inventory inventory, 
            RouletteWheelSelection<Waypoint> roulette, 
            float hidingTime, 
            WaypointsPathScript path) : base(owner, stateMachine)
        {
            _inventory = inventory;
            _seek = seek;
            _roulette = roulette;
            _hidingTime = hidingTime;
            _path = path;
        }

        public override void Awake()
        {
            _chaseOwner = _owner.GetComponent<ChaseEnemy>();
            
            var goal = _roulette.GetRouletWheelResult();
            var start = _path.GetNearestWaypoint(_owner.transform.position);

            if (start != null)
            {
                _currentPath = _path.ExecuteAstar(start, goal);
                _currentTarget = _currentPath.Pop().gameObject;
                _seek.SetTarget(_currentTarget.transform);
                _hidingTimer = _hidingTime;    
            }
        }

        public override void Update()
        {
            var distance = Vector3.Distance(_currentTarget.transform.position, _owner.transform.position);
            if (distance <= 1f)
            {
                if (_currentPath.Count == 0)
                {
                    var items = _inventory.GetItems(Item.ItemType.Key);
                    if (items.Count > 0)
                    {
                        var key = items[0];
                        key.SetPickable();
                        key.Spawn(_owner.transform.position);
                        
                    }
                    _chaseOwner.hasKey = false;
                    _stateMachine.SetState<StatePatrolEnemy>();
                }
                else
                {
                    _currentTarget = _currentPath.Pop().gameObject;
                    _seek.SetTarget(_currentTarget.transform);
                }
            }
        }

        public override void LateUpdate()
        {
            
        }

        public override void Sleep()
        {
            
        }

        
    }
