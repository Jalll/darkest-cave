
    using UnityEngine;
    using UnityEngine.Rendering;

    public class StateGrabKeyEnemy : State<Enemy>
    {

        private Inventory _inventory;
        private Key _key;
        private Seek _seek;
        private float _grabRange;
        private ChaseEnemy _chaseOwner;
        
        public StateGrabKeyEnemy(ChaseEnemy owner, StateMachine<Enemy> stateMachine, Seek seek , Inventory inventory, float grabRange) : base(owner, stateMachine)
        {
            _inventory = inventory;
            _seek = seek;
            _grabRange = grabRange;
        }

        public override void Awake()
        {
            _chaseOwner = _owner.GetComponent<ChaseEnemy>();
            
            var key = _owner.target.GetComponent<Key>();
            if (key == null)
            {
                var player = _owner.target.GetComponent<Player>();
                if (player != null)
                {
                    _stateMachine.SetState<StateChaseEnemy>();
                }
                _stateMachine.SetState<StatePatrolEnemy>();
            }
            else
            {
                _key = key;
                _seek.SetTarget(key.transform);
            }

        }

        public override void Update()
        {
            if (_key.onStorage)
            {
                _stateMachine.SetState<StateHideKeyEnemy>();
            }
            
            var distance = Vector3.Distance(_owner.transform.position, _key.transform.position);
            if (distance <= _grabRange)
            {
                _key.Grab();
                _inventory.SetItem(Item.ItemType.Key, _key);
                _chaseOwner.hasKey = true;
                _stateMachine.SetState<StateHideKeyEnemy>();
            }
        }

        public override void LateUpdate()
        {
            
        }

        public override void Sleep()
        {
            _key = null;
        }
        
        
    }
