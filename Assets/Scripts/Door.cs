﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Door : MonoBehaviour
{
    private const float OPEN_DOOR_TIME = 3f;
    
    
    
    private Vector3 _initialPosition;
    private float _openDoorTimer;
    private bool _openingDoor;

    [SerializeField] private float speed;
    [SerializeField] private GameObject door;
    [SerializeField] private Light light;
    [SerializeField] private Light doorLight;
    
    public UnityEvent OpenDoorEvent;

    void Awake()
    {
        if(OpenDoorEvent == null) OpenDoorEvent = new UnityEvent();
        OpenDoorEvent.AddListener(OpenDoor);

        _initialPosition = door.transform.position;
        
        light.enabled = false;
        doorLight.enabled = false;
    }

    void Update()
    {
        if (_openingDoor)
        {
            _openDoorTimer -= Time.deltaTime;
            door.transform.position += door.transform.up * (speed * Time.deltaTime);
            if (_openDoorTimer <= 0)
            {
                door.gameObject.SetActive(false);
                _openingDoor = false;
            }
        }
    }

    void OpenDoor()
    {
        _openingDoor = true;
        _openDoorTimer = OPEN_DOOR_TIME;
        light.enabled = true;
        doorLight.enabled = true;
    }

    public void Reset()
    {
        door.transform.position = _initialPosition;
        light.enabled = false;
        doorLight.enabled = false;
        door.gameObject.SetActive(true);
    }

}
