namespace DecisionTree
{
    public class QuestionNode : Node
    {
        public delegate bool QuestionDelegate();

        private ActionNode _trueNode;
        private ActionNode _falseNode;
        private QuestionDelegate _question;

        public QuestionNode(QuestionDelegate question, ActionNode positiveActionNode, ActionNode negativeActionNode)
        {
            _question = question;
            _trueNode = positiveActionNode;
            _falseNode = negativeActionNode;
        }
        
        public override void Execute()
        {
            if (_question())
            {
                _trueNode.Execute();
            }
            else
            {
                _falseNode.Execute();
            }
        }
    }
}