namespace DecisionTree
{
    public class ActionNode : Node
    {
        public delegate void Action();

        private Action _action;
        
        public ActionNode(Action action)
        {
            _action = action;
        }

        public override void Execute()
        {
            _action();
        }
    }
}