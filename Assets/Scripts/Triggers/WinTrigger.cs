﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinTrigger : MonoBehaviour
{
    private void OnCollisionEnter(Collision other)
    {
        var player = other.collider.GetComponent<Player>();
        if (player != null)
        {
            if(GameManager.Instance != null)
                GameManager.Instance.winEvent.Invoke();    
        }
    }
}
