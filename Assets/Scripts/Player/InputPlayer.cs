﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputPlayer : MonoBehaviour
{
    
    public Vector3 GetDirectionByInput()
    {
        //var x= Input.GetAxis("HorizontalMovement"); 
        //var z= Input.GetAxis("VerticalMovement");
        var x= Input.GetAxis("HorizontalMovementJ2"); 
        var z= Input.GetAxis("VerticalMovementJ2");
        return new Vector3(x, 0, z).normalized;
    }

    public Vector3 GetViewDirectionByInput()
    {
        var x = Input.GetAxis("HorizontalViewJ2");
        var z = Input.GetAxis("VerticalViewJ2");
        return new Vector3(x, 0, z).normalized;
    }
    
}
