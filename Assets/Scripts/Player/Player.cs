﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public class Player : MonoBehaviour
{
    private const float FLASHLIGHT_BASE_VALUE = 25f;
    
    public Inventory inventory { get; private set; }
    public float energy { get; private set; }
    
    [SerializeField] private Movement movement;
    [SerializeField] private InputPlayer inputPlayer;
    [SerializeField] private Rigidbody rigidBody;
    [SerializeField] private Sensor sensor;
    [SerializeField] private LayerMask enemyLayer;

    [SerializeField] private float initialEnergy;  
    [SerializeField] private Light flashLight;
    
    [Range(0,100)] [SerializeField] private float speed;
    [Range(0,100)][SerializeField] private float rotationSpeed;
    [Range(0, 100)] [SerializeField] private float gravityForce;
    
    
    

    
    void Awake()
    {
        if (movement == null)
        {
            movement = GetComponent<Movement>();
        }

        if (inputPlayer == null)
        {
            inputPlayer = GetComponent<InputPlayer>();
        }
        
        if (inventory == null)
        {
            inventory = GetComponent<Inventory>();
        }

        if (rigidBody == null)
        {
            rigidBody = GetComponent<Rigidbody>();
        }

        if (sensor == null)
        {
            sensor = GetComponent<Sensor>();
        }

        energy = initialEnergy;
        SetFlashlightDistance();
        
    }

    void SetFlashlightDistance()
    {
        var newDistance = FLASHLIGHT_BASE_VALUE * (energy / 100);
        sensor.SetViewDistance(newDistance);
        flashLight.range = newDistance;
    }

    void Update()
    {
        var direction = inputPlayer.GetDirectionByInput();
        movement.Move(direction, speed);

        var rotationDirection = inputPlayer.GetViewDirectionByInput();
        
        movement.Rotate(rotationDirection, rotationSpeed);
            
        rigidBody.AddForce(Physics.gravity * gravityForce, ForceMode.Acceleration);

        var enemies = sensor.GetGameObjectsOnLineOfSight(enemyLayer, transform.position);
        foreach (var enemy in enemies)
        {
            var enemyObject = enemy.GetComponent<Enemy>();
            if (enemyObject != null)
            {
                enemyObject.ActivateLight(gameObject);
            }
        }

    }

    public Item StealKey()
    {
        var keys = inventory.GetItems(Item.ItemType.Key);
        if (keys.Count > 0)
        {
            var key = keys[0];
            inventory.RemoveItem(keys[0]);
            return key;
        }

        return null;
    }

    public void Damage(float damage)
    {

        energy -= damage;
        SetFlashlightDistance();
        if(GameManager.Instance != null)
            GameManager.Instance.checkLoseConditionEvent.Invoke();
    }

    private void OnTriggerEnter(Collider other)
    {
        var item = other.gameObject.GetComponent<Item>();
        if (item != null && item.itemType == Item.ItemType.Key && item.pickable)
        {
            PickUpKey(item);
            var keys = inventory.GetItems(Item.ItemType.Key);
            Debug.Log("KEYS: " + keys.Count);
            if(GameManager.Instance != null)
                GameManager.Instance.checkWinConditionEvent.Invoke();
        }
    }

    private void PickUpKey(Item key)
    {
        key.Grab();
        inventory.SetItem(Item.ItemType.Key, key);
    }
    
    private void DropKey(Item key)
    {
        key.Spawn(transform.position);
        key.SetUnpickable(2);
    }

    public void ForceDropKey()
    {
        var keys = inventory.GetItems(Item.ItemType.Key);
        if (keys.Count > 0)
        {
            Debug.Log("KEYS: " + keys.Count);
            var key = keys[0];
            DropKey(key);
            inventory.RemoveItem(keys[0]);
        }
    }

    public void ResetPlayer()
    {
        inventory.ClearInventory();
        energy = initialEnergy;
        SetFlashlightDistance();
    }
    
}