﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointsPathScript : MonoBehaviour
{
    private const float MAX_RADIUS = 50;

    [SerializeField] public List<Waypoint> _waypoints;
    [SerializeField] private float _maxDistance;
    [SerializeField] private LayerMask _waypointLayer;
    [SerializeField] private LayerMask _obstacleLayer;

    private List<Waypoint> closeWaypoints = new List<Waypoint>();
    private List<Waypoint> openWaypoints = new List<Waypoint>();
    private List<Waypoint> showNodes = new List<Waypoint>();
    
    private void Awake()
    {
        
        if (_waypoints.Count <= 0)
        {
            _waypoints = new List<Waypoint>();
            _waypoints.AddRange(GetComponentsInChildren<Waypoint>());
        }
    }
    
    public Stack<Waypoint> ExecuteAstar(Waypoint initialNode, Waypoint endNode)
    {
        foreach (var waypoint in closeWaypoints)
        {
            waypoint.ResetWaypoint();
        }

        foreach (var waypoint in openWaypoints)
        {
            waypoint.ResetWaypoint();
        }
        
        openWaypoints.Clear();
        closeWaypoints.Clear();
        showNodes.Clear();

        var resultPath = new Stack<Waypoint>();
        
        openWaypoints.Add(initialNode);
        initialNode.SetAsInitialNode();
        
        while (openWaypoints.Count > 0)
        {
            Waypoint currentWaypoint = LookForLowerF();
            
            if (currentWaypoint == endNode)
            {
                while (currentWaypoint != null)
                {
                    resultPath.Push(currentWaypoint);
                    var position = currentWaypoint.transform.position;
                    var previous = currentWaypoint;
                    var compareNode = currentWaypoint.previous;
                    
                    if (compareNode == null) break;
                    
                    var deltaVector = compareNode.transform.position - position;
                    var direction = deltaVector.normalized;
                    var distance = deltaVector.magnitude;
                    // Vector3.Distance(position, compareNode.transform.position);
                    RaycastHit hit;
                    while (!Physics.Raycast(position, direction, out hit, distance, _obstacleLayer))
                    {
                        
                        previous = compareNode;
                        compareNode = compareNode.previous;
                        if (compareNode == null) break;
                        deltaVector = compareNode.transform.position - position;
                        direction = deltaVector.normalized;
                        distance = deltaVector.magnitude;

                    }
                    currentWaypoint = previous;
                    
                }
                closeWaypoints.Add(endNode);
                break;
            }

            foreach (var waypoint  in currentWaypoint.neighbours)
            {
                var neighNode = waypoint.Key;
                var neighDist = waypoint.Value;

                if (closeWaypoints.Contains(neighNode) || neighNode.isBlocked)
                {
                    continue;
                }

                if (!openWaypoints.Contains(neighNode))
                {
                    neighNode.H = HeuristicValue(neighNode, endNode);
                    openWaypoints.Add(neighNode);
                    showNodes.Add(neighNode);

                }

                if (currentWaypoint.G + neighDist < neighNode.G)
                {
                    neighNode.G = currentWaypoint.G + neighDist;
                    neighNode.previous = currentWaypoint;
                }
            }
            
            closeWaypoints.Add(currentWaypoint);
            openWaypoints.Remove(currentWaypoint);
        }
        
        return resultPath;
    }
    
    private Waypoint LookForLowerF()
    {
        var closerNode = openWaypoints[0];
        for (int i = 1; i < openWaypoints.Count; i++)
        {
            var tempNode = openWaypoints[i];
            if (tempNode.F < closerNode.F)
            {
                closerNode = tempNode;
            }
        }

        return closerNode;
    }

    private float HeuristicValue(Waypoint nodeA, Waypoint nodeB)
    {
        return Vector3.Distance(nodeA.transform.position, nodeB.transform.position);
    }
    
    public Waypoint GetNearestWaypoint(Vector3 position)
    {
        Waypoint nearestWaypoint = null;
        int radius = 0;
        float maxDistance = Mathf.Infinity;
        while (nearestWaypoint == null)
        {
            if (radius > MAX_RADIUS) break;
            radius += 5;
            var waypoints = Physics.OverlapSphere(position, radius, _waypointLayer);
            foreach (var found in waypoints)
            {
                var foundWaypoint = found.GetComponent<Waypoint>();
                if (foundWaypoint != null)
                {
                    var distance = Vector3.Distance(position, foundWaypoint.transform.position);
                        
                    RaycastHit hit;

                    if (distance < maxDistance && !Physics.Raycast(position, foundWaypoint.transform.position, out hit,
                        distance, _obstacleLayer))
                    {
                        maxDistance = distance;
                        nearestWaypoint = foundWaypoint;
                    }
                }
            }
        }

        return nearestWaypoint;
    }
    
}
