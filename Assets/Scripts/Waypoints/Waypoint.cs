﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Waypoint : MonoBehaviour
{
    private const float GIZMOS_WAYPOINT_RADIUS = 1f;
    public Waypoint previous;

    public Dictionary<Waypoint, float> neighbours = new Dictionary<Waypoint, float>();
    [SerializeField] private LayerMask nodeLayer;
    [SerializeField] private LayerMask obstacleLayer;

    public float H;
    public float G;
    public float F => H + G;

    public float radius;
    
    public bool isBlocked;
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, GIZMOS_WAYPOINT_RADIUS);

        if (neighbours != null)
        {
            Gizmos.color = Color.red;
            foreach (var neighbour in neighbours.Keys)
            {
                Gizmos.DrawLine(transform.position, neighbour.transform.position);
            }
            
        }
    }

    private void Awake()
    {
        G = Mathf.Infinity;
        GetNeigbours();
    }
    public void ResetWaypoint()
    {
        G = Mathf.Infinity;
        H = Mathf.Infinity;
        previous = null;
    }
    
    public void SetAsInitialNode()
    {
        G = 0;
        previous = null;
    }
    
    private void GetNeigbours()
    {
        var nearNodes = Physics.OverlapSphere(transform.position, radius, nodeLayer);

        foreach (var nodeCollider in nearNodes)
        {
            var node = nodeCollider.GetComponent<Waypoint>();
            //Nos aseguramos de que sea un nodo y que no sea yo mismo. 
            if (node != null &&
                node != this)
            {
                //Conseguimos la distancia hacia nuestro vecino
                var deltaVector = node.transform.position - transform.position;
                var direction = deltaVector.normalized;
                var distance = deltaVector.magnitude;
                RaycastHit hitInfo;
                if (!Physics.Raycast(transform.position, direction, out hitInfo, distance, obstacleLayer))
                {
                    //Lo agregamos al diccionario.
                    neighbours.Add(node, distance);
                }
            }
        }
    }
    
}
