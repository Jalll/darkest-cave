﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Astar : MonoBehaviour
{
    public Waypoint initialNode;
    public Waypoint endNode;

    private List<Waypoint> closeWaypoints = new List<Waypoint>();
    private List<Waypoint> openWaypoints = new List<Waypoint>();
    private Stack<Waypoint> pathWaypoints = new Stack<Waypoint>();
    
    private List<Waypoint> showNodes = new List<Waypoint>();

    public Stack<Waypoint> ExecuteAstar(Waypoint initialNode, Waypoint endNode)
    {
        foreach (var node in closeWaypoints)
        {
            node.ResetWaypoint();
        }

        foreach (var node in openWaypoints)
        {
            node.ResetWaypoint();
        }
        
        openWaypoints.Clear();
        closeWaypoints.Clear();
        showNodes.Clear();

        var resultPath = new Stack<Waypoint>();
        
        openWaypoints.Add(initialNode);
        initialNode.SetAsInitialNode();
        
        while (openWaypoints.Count > 0)
        {
            Waypoint currentWaypoint = LookForLowerF();
            
            if (currentWaypoint == endNode)
            {
                while (currentWaypoint != null)
                {
                    resultPath.Push(currentWaypoint);
                    currentWaypoint = currentWaypoint.previous;
                }
                closeWaypoints.Add(endNode);
                break;
            }

            foreach (var node in currentWaypoint.neighbours)
            {
                var neighNode = node.Key;
                var neighDist = node.Value;

                if (closeWaypoints.Contains(neighNode) || neighNode.isBlocked)
                {
                    continue;
                }

                if (!openWaypoints.Contains(neighNode))
                {
                    neighNode.H = HeuristicValue(neighNode, endNode);
                    openWaypoints.Add(neighNode);
                    showNodes.Add(neighNode);

                }

                if (currentWaypoint.G + neighDist < neighNode.G)
                {
                    neighNode.G = currentWaypoint.G + neighDist;
                    neighNode.previous = currentWaypoint;
                }
            }
            
            closeWaypoints.Add(currentWaypoint);
            openWaypoints.Remove(currentWaypoint);
        }
        
        return resultPath;
    }

    private Waypoint LookForLowerF()
    {
        var closerNode = openWaypoints[0];
        for (int i = 1; i < openWaypoints.Count; i++)
        {
            var tempNode = openWaypoints[i];
            if (tempNode.F < closerNode.F)
            {
                closerNode = tempNode;
            }
        }

        return closerNode;
    }

    private float HeuristicValue(Waypoint nodeA, Waypoint nodeB)
    {
        return Vector3.Distance(nodeA.transform.position, nodeB.transform.position);
    }

}
