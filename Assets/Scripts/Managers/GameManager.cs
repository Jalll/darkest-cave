﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Serialization;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance
    {
        get => _instance;
        private set => _instance = value;
    }
    private static GameManager _instance;
    
    [SerializeField] private List<GameObject> walls;
    [SerializeField] private GameObject PlayerSpawner;
    [SerializeField] private List<GameObject> enemySpawners;
    [SerializeField] private Player player;
    [SerializeField] private List<Enemy> enemies;
    [SerializeField] private Door door;
    [SerializeField] private float winConditionKeys;
    [SerializeField] private List<Key> keys;
    [SerializeField] private List<GameObject> keySpawners;
    
    public UnityEvent winEvent;
    public UnityEvent checkWinConditionEvent;
    public UnityEvent checkLoseConditionEvent;

    private RouletteWheelSelection<GameObject> wallsRoulette = new RouletteWheelSelection<GameObject>();
    private RouletteWheelSelection<GameObject> enemiesSpawnersRoulette = new RouletteWheelSelection<GameObject>();
    private RouletteWheelSelection<GameObject> keysSpawnersRoulette = new RouletteWheelSelection<GameObject>();

    void Awake()
    {
        if (_instance == null) _instance = this;

        if (winEvent == null) winEvent = new UnityEvent();
        if (checkWinConditionEvent == null) checkWinConditionEvent = new UnityEvent();
        if (checkLoseConditionEvent == null) checkLoseConditionEvent = new UnityEvent();
        
        winEvent.AddListener(TriggerWin);
        checkWinConditionEvent.AddListener(CheckWinCondition);
        checkLoseConditionEvent.AddListener(CheckLoseCondition);
        ResetGame();
    }

    void ResetRoullets()
    {
        if (walls != null)
        {
            wallsRoulette.ClearRouletteOptions();
            foreach (var wall in walls)
            {
                wallsRoulette.AddRouletteOption(wall, 1);
            }
        }
        
        if (enemySpawners != null)
        {
            enemiesSpawnersRoulette.ClearRouletteOptions();
            foreach (var spawner in enemySpawners)
            {
                enemiesSpawnersRoulette.AddRouletteOption(spawner, 1);
            }    
        }

        if (keys != null)
        {
            keysSpawnersRoulette.ClearRouletteOptions();
            foreach (var spawner in keySpawners)
            {
                keysSpawnersRoulette.AddRouletteOption(spawner, 1);
            }
        }
    }
    
    void ResetGame()
    {
        ResetRoullets();
        
        player.transform.position = PlayerSpawner.transform.position;
        player.gameObject.SetActive(true);

        foreach (var wall in walls)
        {
            wall.gameObject.SetActive(true);
        }
        
        for (int i = 0; i < 4; i++)
        {
            var wall = wallsRoulette.GetRouletWheelResult();
            if (wall != null)
            {
                wallsRoulette.RemoveRouletteOption(wall);
                wall.SetActive(false);
            }
        }
        
        foreach (var enemy in enemies)
        {
            var spawner = enemiesSpawnersRoulette.GetRouletWheelResult();
            enemiesSpawnersRoulette.RemoveRouletteOption(spawner);

            enemy.transform.position = spawner.transform.position;
            enemy.gameObject.SetActive(true);
            enemy.SetDefaultState();
        }

        foreach (var key in keys)
        {
            var spawner = keysSpawnersRoulette.GetRouletWheelResult();
            keysSpawnersRoulette.RemoveRouletteOption(spawner);

            key.transform.position = spawner.transform.position;
            key.SetPickable();
            key.gameObject.SetActive(true);
        }

        player.ResetPlayer();
        door.Reset();
    }
    
    void TriggerWin()
    {
        ResetGame();
    }

    void CheckWinCondition()
    {
        var keys = player.inventory.GetItems(Item.ItemType.Key);
        if (keys.Count >= winConditionKeys)
        {
            door.OpenDoorEvent.Invoke();
        }
    }

    void CheckLoseCondition()
    {
        if (player.energy <= 0)
        {
            ResetGame();
        }
    }
}
