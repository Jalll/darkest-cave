using UnityEngine;


public class Flee : SteeringBehaviour
{
    protected override void Move()
    {
        Vector3 deltaVector = -(target.transform.position - transform.position);
        direction = deltaVector.normalized;

        movement.Move(direction, speed);
        movement.Rotate(direction, rotationSpeed);
    }
}