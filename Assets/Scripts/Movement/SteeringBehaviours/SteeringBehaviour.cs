﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SteeringBehaviour : MonoBehaviour
{
    [SerializeField] public bool isActivated = true;
    [SerializeField] protected bool useAvoidance;
    [SerializeField] protected LayerMask obstacleLayer;
    [SerializeField] protected ObstacleAvoidance obstacleAvoidance;
    
    [SerializeField] protected float speed;
    [SerializeField] protected float rotationSpeed;
    [SerializeField] protected Movement movement;

    
    protected Transform target;
    protected Vector3 direction;
    

    protected abstract void Move();

    protected virtual void FixedUpdate()
    {
        if (target != null && isActivated)
        {
            Move();   
        }
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void SetTarget(Transform target)
    {
        this.target = target;
    }
}
