﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seek : SteeringBehaviour
{
    protected override void Move()
    {
        if (useAvoidance)
        {
            obstacleAvoidance.Avoidance(obstacleLayer);
            direction = obstacleAvoidance.Movement(target);
        }
        else
        {
            Vector3 deltaVector = target.transform.position - transform.position;
            direction = deltaVector;
        }
        
        transform.forward = direction;
        transform.position += transform.forward * (speed * Time.deltaTime);
        
        //movement.Move(direction, speed);
        //movement.Rotate(direction, rotationSpeed);
        //transform.position += transform.forward * (speed * Time.deltaTime);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawRay(transform.position, direction);
    }
}
