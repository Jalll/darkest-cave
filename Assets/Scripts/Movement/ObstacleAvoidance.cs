﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleAvoidance : MonoBehaviour
{
    
    [Range(0, 100)][SerializeField] private float speed;
    [Range(0, 100)][SerializeField] private float rotationSpeed;
    [Range(0, 100)][SerializeField] private float radius;

    private Transform closerObstacle;
    private Vector3 direction;

    public Vector3 Movement(Transform target)
    {
        var position = transform.position;
        position.y = 0;
        
        var targetPosition = target.transform.position;
        targetPosition.y = 0;
        
        
        direction = targetPosition - position;
        if (closerObstacle != null)
        {
            var closerObstaclePosition = closerObstacle.transform.position;
            closerObstaclePosition.y = 0;
            
            direction -= closerObstaclePosition - position;
        }

        return direction;
    }

    public void Avoidance(LayerMask obstacleLayer)
    {
        closerObstacle = null;

        // Conseguimos todos nuestros obstaculos
        Collider[] overlapSphere = Physics.OverlapSphere(transform.position, radius, obstacleLayer, QueryTriggerInteraction.Ignore);
        if (overlapSphere.Length > 0)
        {
            //Tomamos el primero como nuestro posible obstaculo más cercano.
            closerObstacle = overlapSphere[0].transform;
            //Conseguimos su distancia a nuestro personaje.
            float closerDistance = Vector3.Distance(overlapSphere[0].transform.position, transform.position);

            //Revisamos el resto de los objetos para buscar cual es el más cercano
            for (int i = 1; i < overlapSphere.Length; i++)
            {
                var posibleCloserObstacle = Vector3.Distance(overlapSphere[i].transform.position, transform.position);
                // Si encontramos un más cercano
                if (posibleCloserObstacle < closerDistance)
                {
                    //Lo reemplazamos
                    closerObstacle = overlapSphere[i].transform;
                }
            }
        }
    }

}
