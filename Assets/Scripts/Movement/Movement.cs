﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class Movement : MonoBehaviour
{
    public Vector3 direction { get; private set; }

    public void Move(Vector3 dir, float speed)
    {
        direction += dir.normalized * (speed * Time.deltaTime);
    }

    public void Rotate(Vector3 rotationDirection, float rotationSpeed)
    {
        var newForward = Vector3.Lerp(transform.forward,
            rotationDirection,
            Time.deltaTime * rotationSpeed);
        newForward.y = 0;

        transform.forward = newForward;
    }

    void Update()
    {
        transform.Translate(direction, Space.World);
        direction = Vector3.zero;
    }

}
