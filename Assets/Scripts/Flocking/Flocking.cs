﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Flocking : MonoBehaviour
{
    public Transform leaderTarget;
    public LayerMask neighbourLayers;
    public float neighbourRadius;
    public float speed;
    public float rotationSpeed;
    public float leaderOffset;

    public float alighmentWeight = 1;
    public float cohesionWeight = 1;
    public float separationWeight = 1;
    public float leaderWeight = 1;

    public bool isActivated = true;

    private List<Collider> neighbours = new List<Collider>();
    private Vector3 alignmentVector = new Vector3();
    private Vector3 cohesionVector = new Vector3();
    private Vector3 separationVector = new Vector3();
    private Vector3 leaderVector = new Vector3();

    private void Awake()
    {
        StartCoroutine(GetNeighboursCoroutine());
    }

    private void Update()
    {
        if (!isActivated) return;
        Movement();
    }

    public void CalculateFlocking()
    {
        GetNeigbours();
        Movement();
    }

    private void Movement()
    {
        alignmentVector = CalculateAlignment() * alighmentWeight;
        cohesionVector = CalculateCohesion() * cohesionWeight;
        separationVector = CalculateSeparation() * separationWeight;
        leaderVector = CalculateLeader() * leaderWeight;
        Vector3 direction = (alignmentVector + cohesionVector + separationVector + leaderVector).normalized;

        var aux = Vector3.Lerp(transform.forward, direction, rotationSpeed * Time.deltaTime);
        // aux.x = 0;
        // aux.z = 0;
        aux.y = 0;
        transform.forward = aux;

        transform.position += transform.forward * (speed * Time.deltaTime);
    }

    //La alineación se encarga de que la dirección de todas las unidades se vayan pareciendo a través del tiempo. 
    //Su objetivo es hacer que las unidades miren todas en la misma dirección.  
    private Vector3 CalculateAlignment()
    {
        if (neighbours.Count == 0)
        {
            return Vector3.zero;
        }

        Vector3 alignment = new Vector3();

        foreach (var neigh in neighbours)
        {
            if(neigh != null) alignment += neigh.transform.forward;
        }

        alignment /= neighbours.Count;

        return alignment;
    }

    //La cohersión busca hacer que todas las unidades se muevan en base a un punto en común.
    //Su objetivo es hacer que todas las unidades permanezcan unidas a un punto.
    private Vector3 CalculateCohesion()
    {
        if (neighbours.Count == 0)
        {
            return Vector3.zero;
        }

        Vector3 cohesion = new Vector3();

        foreach (var neigh in neighbours)
        {
            if(neigh != null) cohesion += neigh.transform.position;
        }

        cohesion /= neighbours.Count;
        cohesion = cohesion - transform.position; //B - A 

        return cohesion;
    }

    //La separación se encargar de separar las unidades entre sí.
    //Su objetivo es hacer quem entre sí, se encuentren a una distancia predeterminada.
    private Vector3 CalculateSeparation()
    {
        if (neighbours.Count == 0)
        {
            return Vector3.zero;
        }

        Vector3 separation = new Vector3();
        foreach (var neigh in neighbours)
        {
            if (neigh != null) continue;
            Vector3 neighSeparation = transform.position - neigh.transform.position;
            float reverseMagnitude = Mathf.Abs(neighbourRadius - neighSeparation.magnitude);
            neighSeparation = neighSeparation.normalized * reverseMagnitude;
            separation += neighSeparation;
        }

        separation /= neighbours.Count;

        return separation;
    }

    //En general, vamos a querer que nuestros grupos de unidades sigan a un líder.
    //El objetivo de este vector va a ser que nuestras unidades sigan a un punto.
    private Vector3 CalculateLeader()
    {
        if (leaderTarget == null)
        {
            return Vector3.zero;
        }

        Vector3 leader = new Vector3();
        leader = (leaderTarget.position - leaderTarget.transform.forward * leaderOffset) - transform.position;

        if (leader.magnitude > neighbourRadius)
        {
            leader = leader.normalized * neighbourRadius;
        }

        return leader;
    }

    private void GetNeigbours()
    {
        neighbours.Clear();
        var myNeighbours = Physics.OverlapSphere(transform.position, neighbourRadius, neighbourLayers, QueryTriggerInteraction.Ignore);
        neighbours.AddRange(myNeighbours);
        neighbours.Remove(GetComponent<Collider>());
    }

    IEnumerator GetNeighboursCoroutine()
    {
        while (true)
        {
            GetNeigbours();
            yield return new WaitForSeconds(1);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawRay(transform.position, alignmentVector);

        Gizmos.color = Color.white;
        Gizmos.DrawRay(transform.position, cohesionVector);

        Gizmos.color = Color.black;
        Gizmos.DrawRay(transform.position, separationVector);

        Gizmos.color = Color.blue;
        Gizmos.DrawRay(transform.position, leaderVector);

        if (leaderTarget != null)
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawSphere(leaderTarget.position - (leaderTarget.transform.forward * leaderOffset), 0.2f);
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, neighbourRadius);
    }
}
