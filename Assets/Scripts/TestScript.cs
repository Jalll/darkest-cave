﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestScript : MonoBehaviour
{
    private float i = 1;
    
    public Waypoint A;
    public Waypoint B;
    public LayerMask obstacleLayer; 
    
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            var deltaVector = B.transform.position - A.transform.position;
            var distance = deltaVector.magnitude;
            var direction = deltaVector.normalized;
            RaycastHit hit;
            var result = Physics.Raycast(A.transform.position, direction, out hit, distance, obstacleLayer);
            // Debug.DrawLine(A.transform.position, deltaVector, Color.blue, 10);
            // Debug.Log(result);
        }
    }
}
