﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class Item : MonoBehaviour
{
    public enum ItemType
    {
        Key
    }

    public bool pickable { get; private set; }
    public ItemType itemType { get; protected set; }
    public bool onStorage { get; private set; }

    //[SerializeField] protected Material defaultMaterial;
    //[SerializeField] protected Material unpickableMaterial;

    protected abstract void SetItemType();

    //[SerializeField] protected Renderer[] componentsRenderer;

    private float _unpickableTime;
    private bool _timerOn;


    private Renderer _renderer;
    

    void Awake()
    {
        _renderer = GetComponent<Renderer>();
        pickable = true;
    }
    
    public void Spawn(Vector3 position)
    {
        onStorage = false;
        transform.position = position;
        gameObject.SetActive(true);
    }

    public void Grab()
    {
        onStorage = true;
        gameObject.SetActive(false);
    }

    public virtual void SetUnpickable(float time)
    {
        pickable = false;
        _timerOn = true;
        //SetMaterial(unpickableMaterial);
    }

    public virtual void SetPickable()
    {
        pickable = true;
        _timerOn = false;
        //SetMaterial(defaultMaterial);
    }

    protected virtual void SetMaterial(Material newMaterial)
    {
        _renderer.material = newMaterial;
    }
    
    protected virtual void CheckTimer()
    {
        if (_timerOn)
        {
            _unpickableTime -= Time.deltaTime;
            if (_unpickableTime <= 0)
            {
                SetPickable();
            }
        }
    }

    void Update()
    {
        CheckTimer();
    }
    
}
