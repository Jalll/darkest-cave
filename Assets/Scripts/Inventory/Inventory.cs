﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Diagnostics;

public class Inventory : MonoBehaviour
{
    private Dictionary<Item.ItemType, List<Item>> _items;

    private void Awake()
    {
        _items = new Dictionary<Item.ItemType, List<Item>>();
        ClearInventory();
    }

    public List<Item> GetItems(Item.ItemType possibleItem)
    {
        return _items[possibleItem];
    }

    public void SetItem(Item.ItemType possibleItems, Item item)
    {
        _items[possibleItems].Add(item);
    }

    public void RemoveItem(Item item)
    {
        _items[item.itemType].Remove(item);
    }

    public void ClearInventory()
    {
        if(_items.Count > 0) _items.Clear();
        foreach (var item in EnumUtil.GetValues<Item.ItemType>())
            _items[item] = new List<Item>();
    }

}
