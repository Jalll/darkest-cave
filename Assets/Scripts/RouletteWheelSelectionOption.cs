﻿using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using Random = UnityEngine.Random;

public class RouletteWheelSelection<T> 
{

    Dictionary<T, float> _rouletteOptions = new Dictionary<T, float>();

    private float _totalWeight;

    private void GetTotalWeight()
    {
        float result = 0f;
        
        foreach (var rouletteOption in _rouletteOptions)
        {
            result += rouletteOption.Value;
        }

        _totalWeight = result;
    }
    
    public T GetRouletWheelResult()
    {
        var randomValue = Random.Range(0f, _totalWeight);
        
        foreach (var rouletteOption in _rouletteOptions)
        {
            randomValue -= rouletteOption.Value;

            if (randomValue <= 0)
            {
                return rouletteOption.Key;
            }
        }

        return default;
    }

    public void AddRouletteOption(T obj, float weight)
    {
        _rouletteOptions.Add(obj, weight);
        GetTotalWeight();
    }

    public void RemoveRouletteOption(T obj)
    {
        _rouletteOptions.Remove(obj);
        GetTotalWeight();
    }

    public void ClearRouletteOptions()
    {
        _rouletteOptions.Clear();
    }
    
}