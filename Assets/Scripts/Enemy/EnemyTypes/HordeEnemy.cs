using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class HordeEnemy : Enemy
{
    [SerializeField] private GameObject minionPrefab;
    
    [SerializeField] private Stack<HordeMinionEnemy> minions = new Stack<HordeMinionEnemy>();
    [SerializeField] private Seek seek;
    [SerializeField] private WaypointsPathScript path;
    [SerializeField] private LayerMask playerLayer;
    [SerializeField] private float attackSpeed;
    [SerializeField] private float rechargeTime;
    [SerializeField] private int maxMinions;
    
    private void Start()
    {
        RouletteWheelSelection<Waypoint> roulette = new RouletteWheelSelection<Waypoint>();

        foreach (var waypoint in path._waypoints)
        {
            roulette.AddRouletteOption(waypoint,1);
        }

        for (int i = 0; i < Random.Range(1,3); i++)
        {
            SpawnMinion();
        }
        
        stateMachine = new StateMachine<Enemy>();
        StatePatrolEnemyHorde patrol = new StatePatrolEnemyHorde(this, stateMachine, path, seek, sensor, playerLayer, roulette);
        StateAttackEnemyHorde attack = new StateAttackEnemyHorde(this, stateMachine, minions, attackSpeed, seek, movement, sensor, playerLayer);
        StateHideEnemyHorde hide = new StateHideEnemyHorde(this, stateMachine, seek, roulette, path);
        StateRechargeMinionsEnemyHorde recharge = new StateRechargeMinionsEnemyHorde(this, stateMachine, rechargeTime, maxMinions, minions, seek);
        
        stateMachine.AddState(patrol);
        stateMachine.AddState(attack);
        stateMachine.AddState(hide);
        stateMachine.AddState(recharge);
        
        stateMachine.SetState<StatePatrolEnemyHorde>();
    }
    private void Update()
    {
        base.Update();
        
        stateMachine.Update();
    }


    public override void SetDefaultState()
    {
        if (stateMachine != null)
        {
            stateMachine.SetState<StatePatrolEnemyHorde>();
            DestroyMinions();
            for (int i = 0; i < Random.Range(1, 3); i++)
            {
                SpawnMinion();
            }
        }
    }

    private void DestroyMinions()
    {
        foreach (var minion in minions)
        {
            Destroy(minion);
        }
    }

    public void SpawnMinion()
    {
        var spawnPoint = transform.position - transform.forward; //* Random.Range(0.5f, 1f);
        spawnPoint.y = transform.position.y;
        
        var newEntity = Instantiate(minionPrefab, spawnPoint , transform.rotation);
        var newMinion = newEntity.GetComponent<HordeMinionEnemy>();
        newMinion.SetLeader(gameObject);
        minions.Push(newMinion);
    }
}