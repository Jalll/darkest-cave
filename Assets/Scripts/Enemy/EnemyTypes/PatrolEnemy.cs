
using System;
using DecisionTree;
using UnityEngine;
using Node = UnityEditor.Experimental.GraphView.Node;

public class PatrolEnemy : Enemy
{
    private QuestionNode _initialNode;
    private Transform _currentTarget;
    private Transform _currentWaypoint;
    private Player _player;
    private float _followTarget;
    private int _countWaypoints = 0;
    private RouletteWheelSelection<Waypoint> roulette = new RouletteWheelSelection<Waypoint>();
    
    [Range(0,100)][SerializeField] private float speed;
    
    [SerializeField] private Transform[] waypoints;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private Seek seek;
    [SerializeField] private float attackRange;
    [SerializeField] private float seekTime;
    
    
    private void Awake()
    {
        foreach (var waypoint in waypoints)
        {
            roulette.AddRouletteOption(waypoint.GetComponent<Waypoint>(), 1);
        }

        if (waypoints.Length > 0)
        {
            _currentWaypoint = roulette.GetRouletWheelResult().transform;
        }

        _currentTarget = _currentWaypoint;

        if (seek == null)
        {
            seek = gameObject.GetComponent<Seek>();
        }
        
        seek.SetTarget(_currentTarget);

        CreateDecisionTree();
    }

    private void CreateDecisionTree()
    {
        _initialNode = new QuestionNode(() =>
            {
                if (_followTarget > 0) return true;
                
                var gameObjectsInProximity = sensor.GetGameObjectsInProximity(playerMask, transform.position);
                if (gameObjectsInProximity.Count > 0)
                {
                    _currentTarget = gameObjectsInProximity[0].transform;
                    seek.SetTarget(_currentTarget);
                    _player = _currentTarget.gameObject.GetComponent<Player>();
                    _followTarget = seekTime;
                    
                    return _player != null;
                }

                var gameObjectsOnLineOfSight = sensor.GetGameObjectsOnLineOfSight(playerMask, transform.position);
                if (gameObjectsOnLineOfSight.Count > 0)
                {
                    _currentTarget = gameObjectsOnLineOfSight[0].transform;
                    seek.SetTarget(_currentTarget);
                    _player = _currentTarget.gameObject.GetComponent<Player>();
                    _followTarget = seekTime;
                    
                    return _player != null;
                }

                _currentTarget = _currentWaypoint;
                return false;
            },
        new ActionNode(FollowPlayer),
        new ActionNode(PatrolWaypoints));
    }

    private void PatrolWaypoints()
    {
        if (Vector3.Distance(_currentTarget.position, transform.position) < 1f)
        {
            _countWaypoints = _countWaypoints > waypoints.Length ? 0 : _countWaypoints + 1;
            _currentTarget = roulette.GetRouletWheelResult().transform;
            seek.SetTarget(_currentTarget);
        }
    }

    private void FollowPlayer()
    {
        if (Vector3.Distance(_currentTarget.position, transform.position) < attackRange)
        {
            Attack();
        }

        _followTarget -= Time.deltaTime;
    }

    private void Attack()
    {
        // _player.GetDamage();
        _player.StealKey();
    }

    private void Update()
    {
        _initialNode.Execute();
    }

    public override void SetDefaultState()
    {
        throw new NotImplementedException();
    }
}
