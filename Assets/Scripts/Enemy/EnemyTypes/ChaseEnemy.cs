﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseEnemy : Enemy
{
    [Range(0, 100)][SerializeField] private float _speed;

    [SerializeField] private Inventory _inventory;
    [SerializeField] private Seek _seek;
    [SerializeField] private float _seekTime;
    [SerializeField] private LayerMask _playerLayer;
    [SerializeField] private LayerMask _keyLayer;

    [SerializeField] private float _hidingTime;
    [SerializeField] private WaypointsPathScript _path;
    [Range(0, 10)] [SerializeField] private float _grabRange;
    [Range(0, 10)] [SerializeField] private float _attackRange;

    public bool hasKey = false;

    private void Start()
    {
        RouletteWheelSelection<Waypoint> roulette = new RouletteWheelSelection<Waypoint>();

        foreach (var waypoint in _path._waypoints)
        {
            roulette.AddRouletteOption(waypoint,1);
        }
        
        _seek.SetSpeed(_speed);
        
        stateMachine = new StateMachine<Enemy>();
        StatePatrolEnemy patrol = new StatePatrolEnemy(this, stateMachine, _path, _seek, sensor, _playerLayer, roulette);
        StateChaseEnemy chase = new StateChaseEnemy(this, stateMachine, _seek, target, _attackRange, _seekTime, _speed);
        StateAttackEnemy attack = new StateAttackEnemy(this, stateMachine, sensor, _attackRange, _keyLayer, _seek, _speed);
        StateGrabKeyEnemy grabKey = new StateGrabKeyEnemy(this, stateMachine, _seek,_inventory, _grabRange);
        StateHideKeyEnemy hideKey = new StateHideKeyEnemy(this, stateMachine, _seek, _inventory, roulette, _hidingTime, _path);
        
        stateMachine.AddState(patrol);
        stateMachine.AddState(chase);
        stateMachine.AddState(attack);
        stateMachine.AddState(grabKey);
        stateMachine.AddState(hideKey);
        
        stateMachine.SetState<StatePatrolEnemy>();
    }

    private void Update()
    {
        base.Update();
        stateMachine.Update();
    }

    public override void SetDefaultState()
    {
        if (stateMachine == null) return;
        stateMachine.SetState<StatePatrolEnemy>();
        _seek.SetSpeed(_speed);
        hasKey = false;
        _inventory.ClearInventory();
    }

    public override void ActivateLight(GameObject gameObject)
    {
        base.ActivateLight(gameObject);
        if (!hasKey)
        {
            target = gameObject;
            stateMachine.SetState<StateChaseEnemy>();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, _grabRange);

        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, _attackRange);

        Gizmos.color = Color.magenta;
        if(target)
            Gizmos.DrawLine(transform.position, target.transform.position);
    }
}