﻿using UnityEngine;

public class HordeMinionEnemy : MonoBehaviour
{
    [SerializeField] private GameObject target;
    [SerializeField] private Flocking flocking;
    [SerializeField] private Seek seek;
    [SerializeField] private float attackRange;
    [SerializeField] private float maxAttackTime;
    [SerializeField] private float baseDamage;

    private bool _isAttacking = false;
    private float _attackTime;
    
    private void Awake()
    {
        if(target != null)
            flocking.leaderTarget = target.transform;
        seek.isActivated = false;
    }

    private void Update()
    {
        if (!_isAttacking) return;
        
        _attackTime -= Time.deltaTime;
        if (_attackTime <= 0)
        {
            Destroy(gameObject);
        }

        if (Vector3.Distance(target.transform.position, transform.position) <= attackRange)
        {
            var player = target.GetComponent<Player>();
            if (player != null)
            {
                player.Damage(baseDamage);
                Destroy(gameObject);
            }
        }

    }

    public void SetLeader(GameObject leader)
    {
        target = leader;
        flocking.leaderTarget = target.transform;
    }

    public void Attack(GameObject player)
    {
        target = player;
        flocking.isActivated = false;
        
        _isAttacking = true;
        _attackTime = maxAttackTime;
        seek.SetTarget(target.transform);
        seek.isActivated = true;
    }
}
