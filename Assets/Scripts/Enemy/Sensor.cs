﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Sensor : MonoBehaviour
{
    [SerializeField] private float proximityRange;
    [SerializeField] private float viewDistance;
    [SerializeField] private float viewAngle;

    [SerializeField] private LayerMask obstacleLayer;

    private RaycastHit _hitInfo;
    private Collider[] _colliersOnProximity = new Collider[100];
    private Collider[] _colliersOnSight = new Collider[100];

    private List<GameObject> _processedGameObjectsOnProximity = new List<GameObject>();
    private List<GameObject> _processedGameObjectOnSight = new List<GameObject>();


    public List<GameObject> GetGameObjectsInProximity(LayerMask layerMask, Vector3 viewPosition)
    {
        _processedGameObjectsOnProximity.Clear();
        
        var found = Physics.OverlapSphereNonAlloc(transform.position, proximityRange, _colliersOnProximity,
            layerMask);
        if(found > 0)
            foreach (var collier in _colliersOnProximity)
            {
                if (collier == null) continue;
                Vector3 targetDir = (collier.transform.position - transform.position).normalized;
                if (!Physics.Raycast(viewPosition, targetDir, proximityRange,
                    obstacleLayer, QueryTriggerInteraction.Ignore))
                {
                    _processedGameObjectsOnProximity.Add(collier.gameObject);
                }
            }

        return _processedGameObjectsOnProximity;
    }

    public List<GameObject> GetGameObjectsOnLineOfSight(LayerMask layerMask, Vector3 viewPosition)
    {
        _processedGameObjectOnSight.Clear();

        var found = Physics.OverlapSphereNonAlloc(transform.position, viewDistance, _colliersOnSight,
            layerMask);
        if(found > 0)
            foreach (var collier in _colliersOnSight)
            {
                if (collier == null) continue;
                float distanceToTarget = Vector3.Distance(transform.position, collier.transform.position);
                if (distanceToTarget <= viewDistance)
                {
                    Vector3 targetDir = (collier.transform.position - transform.position).normalized;
                    float angleToTarget = Vector3.Angle(transform.forward, targetDir);
                    
                    if (viewAngle >= angleToTarget)
                    {
                        if (!Physics.Raycast(viewPosition, targetDir,
                            viewDistance, obstacleLayer))
                        {
                            _processedGameObjectOnSight.Add(collier.gameObject);
                        }
                    }
                }
            }

        return _processedGameObjectOnSight;
    }

    public void SetViewDistance(float distance)
    {
        viewDistance = distance;
    }
    
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        Gizmos.DrawWireSphere(transform.position, proximityRange);

        Gizmos.color = Color.red;
        Vector3 rightLimit = Quaternion.AngleAxis(viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (rightLimit * viewDistance));
        
        Vector3 leftLimit = Quaternion.AngleAxis(-viewAngle, transform.up) * transform.forward;
        Gizmos.DrawLine(transform.position, transform.position + (leftLimit * viewDistance));

        //Gizmos.color = Color.yellow;
        //_processedGameObjectsOnProximity.ForEach(go => Gizmos.DrawLine(transform.position, go.transform.position));
        //Gizmos.color = Color.green;
        //_processedGameObjectOnSight.ForEach(go => Gizmos.DrawLine(transform.position, go.transform.position));
    }
}