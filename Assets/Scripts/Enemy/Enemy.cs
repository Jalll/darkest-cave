﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.GlobalIllumination;

public abstract class Enemy : MonoBehaviour
{
    private const float LIGHT_TIME = 3f;
    private const float LIGHT_INTENSITY_MAX = 10;
    
    [SerializeField] protected Sensor sensor;
    [SerializeField] protected Movement movement;

    [SerializeField] protected LayerMask targetLayerMasks;
    [SerializeField] protected StateMachine<Enemy> stateMachine;

    [SerializeField] protected ObstacleAvoidance obstacleAvoidance;
    [SerializeField] protected Light spotLight;

    private bool _lightOn = false;
    private float _lightTimer;
    public GameObject target;


    protected virtual void Update()
    {
        if (_lightOn)
        {
            _lightTimer -= Time.deltaTime;
            spotLight.intensity = (_lightTimer / LIGHT_TIME) * LIGHT_INTENSITY_MAX;
            if (_lightTimer <= 0)
            {
                _lightOn = false;
                spotLight.gameObject.SetActive(false);
            } 
        }
    }
    
    public void SetTarget(GameObject target)
    {
        this.target = target;
    }

    public virtual void ActivateLight(GameObject target) //target that activates the light
    {
        _lightOn = true;
        
        _lightTimer = LIGHT_TIME;
        spotLight.gameObject.SetActive(true);
        spotLight.intensity = LIGHT_INTENSITY_MAX;
    }

    public abstract void SetDefaultState();
}

